Shader "Custom/UIScrollMultiLayer"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _InitialX ("UV Start X", Range(0, 1)) = 0
        _UVClampX ("UV Proportion X", Range(0, 1)) = 1        
        _HeightProportion ("Height Proportion", Range(0, 1)) = 0.5             
        _RowCount ("Image Rows", Float) = 1
        _OffsetX ("Offset X", Float) = 0              
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }
        LOD 100

        Pass
        {
            Cull Off
            Lighting Off
            ZWrite Off
            Fog { Mode Off }
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag            

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                fixed2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct v2f
            {
                fixed2 uv : TEXCOORD0;                
                float4 vertex : SV_POSITION;
                float4 color : COLOR;                
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed _InitialX;  
            fixed _UVClampX;  
            fixed _HeightProportion;                  
            fixed _RowCount;
            fixed _OffsetX;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                v.uv.x += _OffsetX;                
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color = v.color;                
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {                   
                fixed clampOffsetX = fmod(i.uv.x, _UVClampX) + _InitialX;
                fixed offsetY = i.uv.y - _HeightProportion * fmod(trunc(i.uv.x / _UVClampX), _RowCount);
                fixed2 clampedUVOffset = fixed2(clampOffsetX, offsetY);
                fixed4 col = tex2D(_MainTex, clampedUVOffset);
                
                return col;
            }
            ENDCG
        }
    }
}
