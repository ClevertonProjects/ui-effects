Shader "Custom/UIScrolling"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {} 
        _InitialX ("UV Start X", Range(0, 1)) = 0        
        _UVClampX ("UV Proportion X", Range(0, 1)) = 1        
        _OffsetX ("Offset X", Float) = 0               
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }
        LOD 100

        Pass
        {
            Cull Off
            Lighting Off
            ZWrite Off
            Fog { Mode Off }
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag                        

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                fixed2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct v2f
            {
                fixed2 uv : TEXCOORD0;                
                float4 vertex : SV_POSITION;
                float4 color : COLOR;                
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;               
            fixed _UVClampX;            
            fixed _InitialX;            
            fixed _OffsetX;                   

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);  
                v.uv.x += _OffsetX;                              
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);                
                o.color = v.color;      

                return o;
            }

            float4 frag (v2f i) : SV_Target
            {   
                i.uv.x = fmod(i.uv.x, _UVClampX) + _InitialX;                
                float4 col = tex2D(_MainTex, i.uv);

                return col;
            }
            ENDCG
        }
    }
}
