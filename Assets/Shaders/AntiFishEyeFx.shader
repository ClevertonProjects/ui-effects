Shader "Custom/AntiFishEyeFx"
{
    Properties
    {
        [PerRendererData]_MainTex ("Texture", 2D) = "white" {} 
        _Strength ("Strength", Range(0, 0.5)) = 0.5
        _Intensity ("Intensity", Float) = 1
    }
    SubShader
    {
        Tags 
		{ 
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
        LOD 100

        Pass
        {
            Cull Off
			Lighting Off
			ZWrite Off 			
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag            

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;    
                float4 color : COLOR;              
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Strength;
            float _Intensity;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);      
                o.color = v.color;          
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {   
                float2 p = i.uv;
                float2 center = float2(0.5, 0.5);
                float2 d = p - center;
                float r = sqrt(dot(d, d)); // Pixel distance from center.

                float power = (2.0 * 3,1415 / (2.0 * sqrt(dot(center, center)))) * (_Strength - 0.5);
                float bind = center.y;                
                float2 uv = power < 0 ? center + normalize(d) * atan(r * -power * _Intensity) * bind / atan(-power * bind * _Intensity) : p;                               

                // sample the texture
                fixed4 col = tex2D(_MainTex, uv);                
                return col * i.color.a;
            }
            ENDCG
        }
    }
}
