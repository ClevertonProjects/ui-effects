Shader "Custom/SwirlDistortion"
{
    Properties
    {
        [PerRendererData]_MainTex ("Texture", 2D) = "white" {}
        [HDR]_FadeColor("Fade Color", Color) = (1,1,1,1)
        _ColorTransition("Color Factor", Range(0,1)) = 0
        _Radius ("Radius", Float) = 1
        _Angle ("Angle", Float) = 0
        _Center ("Center", Vector) = (0.5,0.5,0,0)        
    }
    SubShader
    {
        Tags 
        { 
            "Queue"="Transparent"
            "RenderType"="Transparent" 
            "IgnoreProjector"="True"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
        LOD 100

        Pass
        {
            Cull Off
            Lighting Off
            ZWrite Off
            Fog { Mode Off }
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;                
                float4 color : COLOR;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _FadeColor;
            fixed _ColorTransition;
            float _Radius;
            float _Angle;
            fixed4 _Center;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.color = v.color;
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float effectAngle = _Angle * UNITY_PI;
                float2 uv = i.uv - _Center;

                float len = length(uv);
                float angle = atan2(uv.y, uv.x) + effectAngle * smoothstep(_Radius, 0, len);                
                float2 finalUv = float2(cos(angle), sin(angle)) * len + _Center;

                // sample the texture
                fixed4 col = tex2D(_MainTex, finalUv);  
                fixed4 blendColor = col * 0.4 + _FadeColor * 0.6;
                blendColor.a *= col.a;              

                col = lerp(col, blendColor, _ColorTransition);

                // Apply scene components alpha variation.
                col *= i.color.a;

                return col;
            }
            ENDCG
        }
    }
}
