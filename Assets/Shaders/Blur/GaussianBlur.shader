Shader "Custom/GaussianBlur"
{
    Properties
    {
        [PerRendererData]_MainTex ("Texture", 2D) = "white" {}        
    }
    SubShader
    {
        Tags 
        { 
            "Queue"="Transparent"
            "RenderType"="Transparent" 
            "IgnoreProjector"="True"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
        LOD 100

        Pass
        {
            Cull Off
            Lighting Off
            ZWrite Off
            Fog { Mode Off }
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag         

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;                
                float4 color : COLOR;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;  
            float4 _MainTex_ST;			          
            
            static const float offset[3] = { 
                float(0.0), 
                float(4.15), 
                float(9.7)               
            };
            
            static const float weight[3] = {
                float(0.2270270270), 
                float(0.3162162162),
                float(0.0702702703) 
            };                                                

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.color = v.color;
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {                   
                fixed4 col = tex2D(_MainTex, i.uv) * weight[0];                
                
                for(int filter = 1; filter < 3; filter++)
                {
                    float offsetProportion = offset[filter] / 1400;

                    col += (tex2D(_MainTex, (i.uv + float2(0.0, offsetProportion))) + tex2D(_MainTex, (i.uv - float2(0.0, offsetProportion)))) * weight[filter];
                    col += (tex2D(_MainTex, (i.uv + float2(offsetProportion, 0.0))) + tex2D(_MainTex, (i.uv - float2(offsetProportion, 0.0)))) * weight[filter];                                        
                }                               

                col *= 0.6 * i.color.a;
                
                return col;
            }
            ENDCG
        }
    }
}
