Shader "Custom/BluredAntiFishEye"
{
    Properties
    {
        [PerRendererData]_MainTex ("Texture", 2D) = "white" {} 
        _Strength ("Strength", Range(0, 0.5)) = 0.5
        _Intensity ("Intensity", Float) = 1
        _Center ("Image Center", Vector) = (0,0,0,0)    
        _Angle ("Angle", Float) = 1 
    }
    SubShader
    {
        Tags 
		{ 
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
        LOD 100

        Pass
        {
            Cull Off
			Lighting Off
			ZWrite Off 			
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag            

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;    
                float4 color : COLOR;              
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Strength;
            float _Intensity;
            fixed4 _Center;
            fixed _Angle;

            float2 rotateUV (float2 uv, float degrees)
            {
                const float Deg2Rad = (UNITY_PI * 2) / 360;
                float rotationRadians = degrees * Deg2Rad;
                float2 sc = float2(sin(rotationRadians), cos(rotationRadians));                
                float2x2 rotationMatrix = float2x2(sc.y, -sc.x, sc.x, sc.y);               

                uv -= _Center;
                uv = mul(rotationMatrix, uv);
                uv += _Center;

                return uv;
            }            

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);      
                o.color = v.color;          
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {   
                float2 p = i.uv;
                float2 center = float2(0.5, 0.5);
                float2 d = p - center;
                float r = sqrt(dot(d, d)); // Pixel distance from center.

                float power = (2.0 * 3.1415 / (2.0 * sqrt(dot(center, center)))) * (_Strength - 0.5);
                float bind = center.y;                
                float2 uv = power < 0 ? center + normalize(d) * atan(r * -power * _Intensity) * bind / atan(-power * bind * _Intensity) : p;                               

                const float Deg2Rad = (UNITY_PI * 2) / 360;
                const float Rad2Deg = 180 / UNITY_PI;                
                float2 coord = uv;                            
                float illuminationDecay = 1;
                float4 fragColor = float4(0, 0, 0, 0);
                int samp = clamp(_Angle, 1, 360);

                // Blur.
                for (float blurLevel = 0; blurLevel < samp; blurLevel++)
                {
                    coord = rotateUV(coord, _Angle/samp);
                    float4 texel = tex2D(_MainTex, coord);
                    texel *= illuminationDecay * 1 / samp;
                    fragColor += texel;
                }

                fixed4 col = fragColor * i.color.a;                                 
                return col * i.color.a;
            }
            ENDCG
        }
    }
}
