Shader "Custom/ImageCarve"
{
    Properties
    {
        [PerRendererData]_MainTex ("Texture", 2D) = "white" {}
        _CarveConfig("Carve Configuration", Vector) = (0.5, 0.5, 0.2, 0.2)         
        _Smooth ("Smoothness", Range(0, 1)) = 0
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }       

        Pass
        {
            Cull Off
            Lighting Off
            ZWrite Off
            Fog { Mode Off }
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag            

            #include "UnityCG.cginc"
            #include "UnityUI.cginc"

            #pragma multi_compile_local _ UNITY_UI_CLIP_RECT

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;                
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                float4 worldPosition : TEXCOORD1;  
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _CarveConfig;                    
            fixed _Smooth;
            float4 _ClipRect;        

            v2f vert (appdata v)
            {
                v2f o;
                o.worldPosition = v.vertex;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);                
                o.color = v.color;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv) * i.color;  

                fixed xMin = max(0, _CarveConfig.x - _CarveConfig.z);
                fixed xMax = min(1, _CarveConfig.x + _CarveConfig.z);
                fixed yMin = max(0, _CarveConfig.y - _CarveConfig.w);
                fixed yMax = min(1, _CarveConfig.y + _CarveConfig.w);

                fixed minSmoothVert = smoothstep(yMin, yMin - _Smooth, i.uv.y);
                fixed minSmoothHor = smoothstep(xMin, xMin - _Smooth, i.uv.x);
                fixed maxSmoothVert = smoothstep(yMax, yMax + _Smooth, i.uv.y);
                fixed maxSmoothHor = smoothstep(xMax, xMax + _Smooth, i.uv.x);                
                
                fixed shadowFade = 1 - (1 - minSmoothVert) * (1 - minSmoothHor) * (1 - maxSmoothVert) + maxSmoothHor * (1 - maxSmoothVert) * (1 - minSmoothVert);
                col.a *= shadowFade;

                #ifdef UNITY_UI_CLIP_RECT                
                col.a *= UnityGet2DClipping(i.worldPosition.xy, _ClipRect);                
                #endif
                
                return col;
            }
            ENDCG
        }
    }
}
