Shader "Custom/BlobShadowFX"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}        
        _VRadius ("Radius", Range(0, 1)) = 1
        _VSoft ("Softness", Range(0, 1)) = 0.5        
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }
        LOD 100

        Pass
        {
            Cull Off
            Lighting Off
            ZWrite Off
            Fog { Mode Off }
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag            

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;    
                float4 color : COLOR;            
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            fixed4 _MainTex_ST;            
            fixed _VRadius;
            fixed _VSoft;            

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.color = v.color;
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {                         
                fixed4 col = i.color;
                fixed distFromCenter = distance(i.uv.xy, float2(0.5, 0.5));
                fixed shadow = smoothstep(_VRadius, _VRadius - _VSoft, distFromCenter);               
                col.a *= shadow;
                
                return col;
            }
            ENDCG
        }
    }
}
