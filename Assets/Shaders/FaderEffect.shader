﻿Shader "Custom/FaderEffect"
{	
	Properties
	{
		[PerRendererData] _MainTex("Texture", 2D) = "white" {}
		_TransitionTex("Pattern Image", 2D) = "white" {}		
		_Color("Color", Color) = (1,1,1,1)		
		_CutOff("Cut Off", Range(0.0, 1.0)) = 0.0	
		_EdgeSize ("Edge Size", Range(0.001,1)) = 0.5				
	}

	SubShader
	{		
		Tags 
		{ 
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
		LOD 100		

		Pass
		{
			Cull Off
			Lighting Off
			ZWrite Off 			
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag			

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;				
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 uv : TEXCOORD0;				
			};			

			sampler2D _MainTex;
			float4 _MainTex_ST;			
			sampler2D _TransitionTex;
			float4 _TransitionTex_ST;			
			float _CutOff;									
			float _EdgeSize;									
			fixed4 _Color;			
			
			v2f vert (appdata v)
			{
				v2f o;								
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);				
				o.color = v.color * _Color;

				return o;
			}	

			fixed4 frag (v2f i) : SV_Target
			{	
				float2 tiling = i.uv / _TransitionTex_ST.xy + _TransitionTex_ST.zw;
				fixed4 noise = tex2D(_TransitionTex, tiling);				
				fixed4 main = tex2D(_MainTex, i.uv);

				fixed edge = 1 - clamp((noise.r - _CutOff) / _EdgeSize, 0, 1);				
				fixed4 color = fixed4(main.rgb,edge) * i.color;

				return color;
			}
			ENDCG
		}
	}
}
