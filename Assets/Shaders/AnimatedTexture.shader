Shader "Custom/AnimatedTexture"
{
    Properties
    {
        [PerRendererData]_MainTex ("Texture", 2D) = "white" {}
        _Tiles ("Tiles", Vector) = (0.0, 0.0, 0.0, 0.0)
        _AnimationSpeed ("Animation Speed", float) = 0.0
    }
    SubShader
    {
        Tags 
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"            
            "RenderType" = "Transparent"             
        }
        LOD 100

        Pass
        {
            Cull Off
            Lighting Off
            ZWrite Off
            Fog { Mode Off }
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag            

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;                
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _Tiles;
            float _AnimationSpeed;

            float2 getOffsetUV(float2 uv, float2 sheetTiles, float speed)
			{	
				float2 size = float2(1.0 / sheetTiles.x, 1.0 / sheetTiles.y);
				float frameIndex = floor(fmod(_Time.y * speed, sheetTiles.x * sheetTiles.y));
				float2 sheetCoord = float2(fmod(frameIndex, sheetTiles.x), floor(frameIndex / sheetTiles.x));

				float2 offset = float2(size.x * sheetCoord.x, 1.0 - (size.y * sheetCoord.y) - size.y);
				float2 newUv = (uv * size) + offset;                

				return newUv;
			}

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                float2 uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv = getOffsetUV(uv, _Tiles.xy, _AnimationSpeed);
                o.color = v.color;
                
                return o;
            }           

            fixed4 frag (v2f i) : SV_Target
            {                
                fixed4 col = tex2D(_MainTex, i.uv) * i.color;                
                
                return col;
            }
            ENDCG
        }
    }
}
