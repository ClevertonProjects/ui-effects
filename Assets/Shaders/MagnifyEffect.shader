Shader "Custom/MagnifyEffect"
{
    Properties
    {
        [PerRendererData] _MainTex ("Texture", 2D) = "white" {}
        _Center ("Center", Vector) = (0.5,0.5,0,0)        
        _Radius ("Radius", Float) = 0.5
        _Strength ("Strength", Float) = 0.5
    }
    SubShader
    {
        Tags 
		{ 
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
        LOD 100

        Pass
        {
            Cull Off
			Lighting Off
			ZWrite Off 			
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag            

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;                
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Center;
            fixed _Radius;
            fixed _Strength;

            float2 getModifiedUV (float2 actualUV, float2 centerPoint, float radius, float strength)
            {
                float2 pointToCenter = centerPoint - actualUV;
                float dist = length(pointToCenter);

                float mag = (1.0 - (dist / radius)) * strength;
                mag *= step(dist, radius);

                return actualUV + (mag * pointToCenter);
            }

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {                
                float2 uv = getModifiedUV(i.uv, _Center.xy, _Radius, _Strength);

                // sample the texture
                fixed4 col = tex2D(_MainTex, uv);                
                return col;
            }
            ENDCG
        }
    }
}
