Shader "Custom/DissolveEffect"
{
    Properties
    {
        [PerRendererData] _MainTex("Texture", 2D) = "white" {}
		_TransitionTex("Pattern Image", 2D) = "white" {}		
        _Fade ("Fade", Range(0,1)) = 1
        _CutOff ("Cut Off", Range(0,1)) = 0.5
        _EdgeSize ("Edge Size", Range(0,1)) = 0.2
        [HDR]_EdgeColor ("Edge Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags 
		{ 
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}
        LOD 100

        Pass
        {
            Cull Off
			Lighting Off
			ZWrite Off 			
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;                
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _TransitionTex;
            fixed _Fade;
            fixed _CutOff;
            fixed _EdgeSize;
            half4 _EdgeColor;

            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 mainCol = tex2D(_MainTex, i.uv);
                fixed4 noise = tex2D(_TransitionTex, i.uv);  
                fixed cutStepIn = step(_CutOff, noise.r);
                fixed cutStepOut = step(_CutOff + _EdgeSize, noise.r);
                fixed edge = cutStepIn - cutStepOut;
                
                fixed4 edgeColor = mainCol * 0.5 + _EdgeColor * 0.5;                

                fixed4 color = lerp(fixed4(mainCol.r, mainCol.g, mainCol.b, 1-cutStepIn), edgeColor, edge) * _Fade;
                return color;
            }
            ENDCG
        }
    }
}
