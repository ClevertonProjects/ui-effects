using System;
using System.Collections.Generic;
using UnityEngine;

namespace TimetableSystem
{
    public class Timetable : MonoBehaviour
    {
        private List<ITaskOrder> tasksList = new List<ITaskOrder>();
        private float elapsedTime;
        private int tasksAmount;
        private Action onFinished;

        private bool isLoop;
        private bool isPaused;        

        public bool Loop { set { isLoop = value; } }
        public bool IsRunning => enabled;

        public Action OnFinished { set { onFinished = value; } }

        private void Awake()
        {
            enabled = false;
        }

        private void Update()
        {
            int tasksCompleted = 0;

            if (!isPaused)
            {
                foreach (ITaskOrder task in tasksList)
                {
                    if (task.IsOver())
                    {
                        tasksCompleted++;

                        if (tasksCompleted >= tasksAmount)
                        {
                            if (isLoop)
                            {                                
                                elapsedTime = 0f;
                                Reset();
                            }
                            else
                            {
                                onFinished?.Invoke();                                
                                enabled = false;
                            }
                        }
                    }
                    else if (elapsedTime >= task.StartTime)
                    {
                        task.Execute();
                    }
                }

                elapsedTime += Time.deltaTime;
            }
        }        

        public void AddTask(ITaskOrder newTask)
        {
            tasksList.Add(newTask);
        }

        public void Run()
        {
            tasksAmount = tasksList.Count;
            elapsedTime = 0f;
            enabled = true;            
        }

        private void Reset()
        {
            foreach (ITaskOrder task in tasksList)
            {
                task.Reset();
            }
        }

        public void Stop()
        {
            enabled = false;
            elapsedTime = 0f;
            Reset();
        }

        public void Pause()
        {
            isPaused = true;
            enabled = false;
        }

        public void Resume()
        {
            isPaused = false;
            enabled = true;
        }

        public void Clear()
        {
            Stop();
            tasksList.Clear();
            onFinished = null;
        }
    }
}
