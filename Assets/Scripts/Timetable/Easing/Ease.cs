using UnityEngine;

namespace TimetableSystem
{
    public abstract class Ease
    {
        private delegate float EasingFunction(float x);
        private static EasingFunction[] easingFunctions = new EasingFunction[]
        {
             Linear,
             ExpoIn,
             ExpoOut,
             ExpoInOut,
             ExpoOutIn,
             SineIn,
             SineOut,
             SineInOut,
             SineOutIn,
             CircularIn,
             CircularOut,
             CircularInOut,
             CircularOutIn,
             QuadraticIn,
             QuadraticOut,
             QuadraticInOut,
             QuadraticOutIn,
             CubicIn,
             CubicOut,
             CubicInOut,
             CubicOutIn,
             QuarticIn,
             QuarticOut,
             QuarticInOut,
             QuarticOutIn,
             QuinticIn,
             QuinticOut,
             QuinticInOut,
             QuinticOutIn,
             ElasticIn,
             ElasticOut,
             ElasticInOut,
             ElasticOutIn,
             BounceIn,
             BounceOut,
             BounceInOut,
             BounceOutIn,
             BackIn,
             BackOut,
             BackInOut,
             BackOutIn
        };

        private static float Linear(float t)
        {
            return t;
        }

        #region Expo
        private static float ExpoIn(float t)
        {
            return t <= 0f ? 0f : Mathf.Pow(2, 10 * (t - 1));
        }

        private static float ExpoOut(float t)
        {
            return t >= 1f ? t : 1 - Mathf.Pow(2, -10 * t);
        }

        private static float ExpoInOut(float t)
        {
            if (t <= 0f) return 0f;
            if (t >= 1f) return 1f;

            if (t < 0.5f)
            {
                return 0.5f * Mathf.Pow(2, (20 * t) - 10);
            }
            else
            {
                return -0.5f * Mathf.Pow(2, (-20 * t) + 10) + 1;
            }
        }

        private static float ExpoOutIn(float t)
        {
            if (t <= 0f) return 0f;
            if (t >= 1f) return 1f;

            if (t < 0.5f)
            {                
                return 0.5f * (1 - Mathf.Pow(2, (-10 * (t * 2))));
            }
            else
            {                
                return 0.5f * Mathf.Pow(2, 10 * ((2 * t - 1) - 1)) + 0.5f;
            }
        }
        #endregion

        #region Sine
        private static float SineIn(float t)
        {
            return Mathf.Sin((t - 1) * Mathf.PI / 2) + 1;
        }

        private static float SineOut(float t)
        {
            return Mathf.Sin(t * Mathf.PI / 2);
        }

        private static float SineInOut(float t)
        {
            return 0.5f * (1 - Mathf.Cos(t * Mathf.PI));
        }

        private static float SineOutIn(float t)
        {
            if (t <= 0.5f)
            {
                return 0.5f * Mathf.Sin(t * 2 * Mathf.PI / 2);
            }
            else
            {
                return 0.5f * (Mathf.Sin(2 * (t - 1) * Mathf.PI / 2) + 1) + 0.5f;
            }
        }
        #endregion

        #region Circular
        private static float CircularIn(float t)
        {
            return 1 - Mathf.Sqrt(1 - (t * t));
        }

        private static float CircularOut(float t)
        {
            return Mathf.Sqrt((2 - t) * t);
        }

        private static float CircularInOut(float t)
        {
            if (t < 0.5f)
            {
                return 0.5f * (1 - Mathf.Sqrt(1 - 4 * (t * t)));
            }
            else
            {
                return 0.5f * (Mathf.Sqrt(-((2 * t) - 3) * ((2 * t) - 1)) + 1);
            }
        }

        private static float CircularOutIn(float t)
        {
            if (t < 0.5f)
            {
                return 0.5f * Mathf.Sqrt((2 - (t * 2)) * (t * 2));
            }
            else
            {
                t = 2 * t - 1;
                return 0.5f * (1 - Mathf.Sqrt(1 - t * t)) + 0.5f;
            }
        }
        #endregion

        #region Quadratic
        private static float QuadraticIn(float t)
        {
            return t * t;
        }

        private static float QuadraticOut(float t)
        {            
            return -(t * (t - 2));
        }

        private static float QuadraticInOut(float t)
        {
            if (t < 0.5f)
            {
                return 2 * t * t;
            }
            else
            {
                return (-2 * t * t) + (4 * t) - 1;
            }
        }

        private static float QuadraticOutIn(float t)
        {
            if (t <= 0.5f)
            {                
                return -t * (2 * t - 2);
            }
            else
            {                
                return 0.5f * (2 * t - 1) * (2 * t - 1) + 0.5f;
            }
        }
        #endregion

        #region Cubic
        private static float CubicIn(float t)
        {
            return t * t * t;
        }

        private static float CubicOut(float t)
        {
            float x = t - 1;
            return x * x * x + 1;
        }

        private static float CubicInOut(float t)
        {
            if (t < 0.5f)
            {
                return 4 * t * t * t;
            }
            else
            {
                float x = ((2 * t) - 2);
                return 0.5f * x * x * x + 1;
            }            
        }

        private static float CubicOutIn(float t)
        {
            if (t < 0.5f)
            {
                float x = (t * 2) - 1;
                return 0.5f * (x * x * x + 1.0f);
            }
            else
            {
                t = 2.0f * t - 1.0f;
                return 0.5f * (t * t * t) + 0.5f;
            }
        }
        #endregion

        #region Quartic
        private static float QuarticIn(float t)
        {
            return t * t * t * t;
        }

        private static float QuarticOut(float t)
        {
            float x = t - 1;
            return x * x * x * (1 - t) + 1;
        }

        private static float QuarticInOut(float t)
        {
            if (t < 0.5f)
            {
                return 8 * t * t * t * t;
            }
            else
            {
                float x = t - 1;
                return -8 * x * x * x * x + 1;
            }
        }

        private static float QuarticOutIn(float t)
        {
            if (t < 0.5f)
            {
                float x = (t * 2) - 1;
                return 0.5f * (x * x * x * (1 - t * 2) + 1);
            }
            else
            {
                t = 2 * t - 1;
                return 0.5f * (t * t * t * t) + 0.5f;
            }
        }
        #endregion

        #region Quintic
        private static float QuinticIn(float t)
        {
            return t * t * t * t * t;
        }

        private static float QuinticOut(float t)
        {
            float x = t - 1;
            return x * x * x * x * x + 1;
        }

        private static float QuinticInOut(float t)
        {
            if (t < 0.5f)
            {
                return 16 * t * t * t * t * t;
            }
            else
            {
                float x = (2 * t) - 2;
                return 0.5f * x * x * x * x * x + 1;
            }
        }

        private static float QuinticOutIn(float t)
        {
            if (t < 0.5f)
            {
                float x = (t * 2) - 1;
                return 0.5f * (x * x * x * x * x + 1);
            }
            else
            {
                t = 2 * t - 1;
                return 0.5f * (t * t * t * t * t) + 0.5f;
            }
        }
        #endregion

        #region Elastic
        private static float ElasticIn(float t)
        {
            return Mathf.Sin(13 * Mathf.PI / 2 * t) * Mathf.Pow(2, 10 * (t - 1));
        }

        private static float ElasticOut(float t)
        {
            return Mathf.Sin(-13 * Mathf.PI / 2 * (t + 1)) * Mathf.Pow(2, -10 * t) + 1;
        }

        private static float ElasticInOut(float t)
        {
            if (t < 0.5f)
            {
                return 0.5f * Mathf.Sin(13 * Mathf.PI / 2 * 2 * t) * Mathf.Pow(2, 10 * ((2 * t) - 1));
            }
            else
            {
                return 0.5f * (Mathf.Sin(-13 * Mathf.PI / 2 * ((2 * t - 1) + 1)) * Mathf.Pow(2, -10 * (2 * t - 1)) + 2);
            }
        }

        private static float ElasticOutIn(float t)
        {
            if (t < 0.5f)
            {
                float x = t * 2f;
                return 0.5f * (Mathf.Sin(-13 * Mathf.PI / 2 * (x + 1)) * Mathf.Pow(2, -10 * x) + 1);
            }
            else
            {
                t = 2 * t - 1;
                return 0.5f * (Mathf.Sin(13 * Mathf.PI / 2 * t) * Mathf.Pow(2, 10 * (t - 1))) + 0.5f;
            }
        }
        #endregion

        #region Bounce
        private static float BounceIn(float t)
        {
            return 1 - BounceOut(1 - t);
        }

        private static float BounceOut(float t)
        {
            if (t < 4f / 11f)
            {
                return (121 * t * t) / 16f;
            }
            else if (t < 8f / 11f)
            {
                return (9.075f * t * t) - (9.9f * t) + 3.4f;
            }
            else if (t < 9f / 10f)
            {
                return (12.066482f * t * t) - (19.635457f * t) + 8.898061f;
            }
            else
            {
                return (10.8f * t * t) - (20.52f * t) + 10.72f;
            }
        }

        private static float BounceInOut(float t)
        {
            if (t < 0.5f)
            {
                return 0.5f * BounceIn(t * 2);
            }
            else
            {
                return 0.5f * BounceOut(t * 2 - 1) + 0.5f;
            }
        }

        private static float BounceOutIn(float t)
        {
            if (t < 0.5f)
            {
                float x = t * 2;
                return 0.5f * BounceOut(x);
            }
            else
            {
                t = 2 * t - 1;
                return 0.5f * (BounceIn(t)) + 0.5f;
            }
        }
        #endregion

        #region Back
        private static float BackIn(float t)
        {
            return t * t * t - t * Mathf.Sin(t * Mathf.PI);
        }

        private static float BackOut(float t)
        {
            float x = 1 - t;
            return 1 - (x * x * x - x * Mathf.Sin(x * Mathf.PI));
        }

        private static float BackInOut(float t)
        {
            float x;

            if (t < 0.5f)
            {
                x = 2 * t;
                return 0.5f * (x * x * x - x * Mathf.Sin(x * Mathf.PI));
            }
            else
            {
                x = 1 - (2 * t - 1);
                return 0.5f * (1 - (x * x * x - x * Mathf.Sin(x * Mathf.PI))) + 0.5f;
            }
        }

        private static float BackOutIn(float t)
        {
            if (t < 0.5f)
            {
                float x = 2 * t;
                return 0.5f * BackOut(x);
            }
            else
            {
                t = 2 * t - 1;
                return 0.5f * BounceIn(t) + 0.5f;
            }
        }
        #endregion

        public static float Value(float t, EaseMode easeMode)
        {
            int easeIndex = (int)easeMode;
            float result = easingFunctions[easeIndex](t);            

            return result;
        }

    }
}
