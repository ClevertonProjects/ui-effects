using UnityEngine;

namespace TimetableSystem
{
    public struct MaterialPropertyTask : ITaskOrder
    {
        private enum MaterialPropertyType { pInt, pFloat, pVector, pColor };
        private readonly MaterialPropertyType propertyType;

        private readonly Material material;
        private readonly int propertyId;
        private MaterialPropertyData originValue;
        private readonly MaterialPropertyData targetValue;
        
        private bool isOriginInitialized;
        
        private readonly float duration;
        private readonly float startTime;
        private float elapsedTime;
        private readonly EaseMode easing;

        public MaterialPropertyTask(Material material, int propertyId, int to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            this.material = material;
            this.propertyId = propertyId;
            propertyType = MaterialPropertyType.pInt;
            originValue = new MaterialPropertyData();
            targetValue = new MaterialPropertyData() { ValueInt = to };

            isOriginInitialized = false;
            this.duration = duration;
            this.startTime = startTime;
            elapsedTime = 0f;
            this.easing = easing;
        }

        public MaterialPropertyTask(Material material, int propertyId, float to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            this.material = material;
            this.propertyId = propertyId;
            propertyType = MaterialPropertyType.pFloat;
            originValue = new MaterialPropertyData();
            targetValue = new MaterialPropertyData() { ValueFloat = to };

            isOriginInitialized = false;
            this.duration = duration;
            this.startTime = startTime;
            elapsedTime = 0f;
            this.easing = easing;
        }

        public MaterialPropertyTask(Material material, int propertyId, Vector4 to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            this.material = material;
            this.propertyId = propertyId;
            propertyType = MaterialPropertyType.pVector;
            originValue = new MaterialPropertyData();
            targetValue = new MaterialPropertyData() { ValueVector = to };

            isOriginInitialized = false;
            this.duration = duration;
            this.startTime = startTime;
            elapsedTime = 0f;
            this.easing = easing;
        }

        public MaterialPropertyTask(Material material, int propertyId, Color to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            this.material = material;
            this.propertyId = propertyId;
            propertyType = MaterialPropertyType.pColor;
            originValue = new MaterialPropertyData();
            targetValue = new MaterialPropertyData() { ValueColor = to };

            isOriginInitialized = false;
            this.duration = duration;
            this.startTime = startTime;
            elapsedTime = 0f;
            this.easing = easing;
        }

        public float StartTime => startTime;

        public float Duration => duration;

        public float ElapsedTime => elapsedTime;

        private void InitializeOrigin()
        {
            isOriginInitialized = true;

            switch (propertyType)
            {
                case MaterialPropertyType.pInt:
                    originValue.ValueInt = material.GetInt(propertyId);
                    break;

                case MaterialPropertyType.pFloat:
                    originValue.ValueFloat = material.GetFloat(propertyId);
                    break;

                case MaterialPropertyType.pVector:
                    originValue.ValueVector = material.GetVector(propertyId);
                    break;

                case MaterialPropertyType.pColor:
                    originValue.ValueColor = material.GetColor(propertyId);
                    break;

                default:
                    break;
            }
        }

        private void LerpProperty(float proportion)
        {
            switch (propertyType)
            {
                case MaterialPropertyType.pInt:
                    int intValue = Mathf.RoundToInt(Mathf.LerpUnclamped(originValue.ValueInt, targetValue.ValueInt, proportion));
                    material.SetInt(propertyId, intValue);
                    break;

                case MaterialPropertyType.pFloat:
                    float floatValue = Mathf.LerpUnclamped(originValue.ValueFloat, targetValue.ValueFloat, proportion);
                    material.SetFloat(propertyId, floatValue);
                    break;

                case MaterialPropertyType.pVector:
                    Vector4 vectorValue = Vector4.LerpUnclamped(originValue.ValueVector, targetValue.ValueVector, proportion);
                    material.SetVector(propertyId, vectorValue);
                    break;

                case MaterialPropertyType.pColor:
                    Color colorValue = Color.LerpUnclamped(originValue.ValueColor, targetValue.ValueColor, proportion);
                    material.SetColor(propertyId, colorValue);
                    break;

                default:
                    break;
            }
        }

        public void Execute()
        {
            if (!isOriginInitialized)
            {
                InitializeOrigin();
            }

            elapsedTime += Time.deltaTime;
            
            float t = Ease.Value(TimelapseProportion(), easing);
            LerpProperty(t);

            if (IsOver())
            {
                SetFinalValue();
            }
            
        }

        private void SetFinalValue()
        {
            switch (propertyType)
            {
                case MaterialPropertyType.pInt:                    
                    material.SetInt(propertyId, targetValue.ValueInt);
                    break;

                case MaterialPropertyType.pFloat:                    
                    material.SetFloat(propertyId, targetValue.ValueFloat);
                    break;

                case MaterialPropertyType.pVector:                    
                    material.SetVector(propertyId, targetValue.ValueVector);
                    break;

                case MaterialPropertyType.pColor:                    
                    material.SetColor(propertyId, targetValue.ValueColor);
                    break;

                default:
                    break;
            }
        }

        public bool IsOver()
        {
            return elapsedTime > duration;
        }

        public void Reset()
        {
            elapsedTime = 0f;            
        }

        public float TimelapseProportion()
        {
            return duration <= 0f ? 1f : elapsedTime / duration;
        }
    }
}
