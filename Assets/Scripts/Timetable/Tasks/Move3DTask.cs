using UnityEngine;
using System.Collections.Generic;

namespace TimetableSystem
{
    public struct Move3DTask : ITaskOrder
    {
        private readonly Transform[] transforms;
        private readonly Vector3[] origins;
        private readonly float targetX;
        private readonly float targetY;
        private readonly float targetZ;

        private readonly bool moveX;
        private readonly bool moveY;
        private readonly bool moveZ;
        private bool isCoordinateLocal;

        private bool isOriginInitialized;

        private readonly float duration;
        private readonly float startTime;
        private float elapsedTime;
        private readonly EaseMode easing;
        
        public Move3DTask(Transform transform, VectorData to, float duration, float startTime, bool isLocal = false, EaseMode easing = EaseMode.Linear)
        {
            transforms = new Transform[] { transform };
            origins = new Vector3[1];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> coordinates = to.GetCoordinates();
            moveX = coordinates.ContainsKey("x");
            moveY = coordinates.ContainsKey("y");
            moveZ = coordinates.ContainsKey("z");
            isCoordinateLocal = isLocal;

            targetX = moveX ? coordinates["x"] : 0f;
            targetY = moveY ? coordinates["y"] : 0f;
            targetZ = moveZ ? coordinates["z"] : 0f;
        }

        public Move3DTask(Transform[] transforms, VectorData to, float duration, float startTime, bool isLocal = false, EaseMode easing = EaseMode.Linear)
        {
            this.transforms = transforms;
            origins = new Vector3[transforms.Length];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> coordinates = to.GetCoordinates();
            moveX = coordinates.ContainsKey("x");
            moveY = coordinates.ContainsKey("y");
            moveZ = coordinates.ContainsKey("z");
            isCoordinateLocal = isLocal;

            targetX = moveX ? coordinates["x"] : 0f;
            targetY = moveY ? coordinates["y"] : 0f;
            targetZ = moveZ ? coordinates["z"] : 0f;            
        }

        public float Duration => duration;
        public float StartTime => startTime;
        public float ElapsedTime => elapsedTime;

        private void InitializeOrigins()
        {
            isOriginInitialized = true;

            for (int i = 0; i < origins.Length; i++)
            {
                if (isCoordinateLocal)
                {
                    origins[i] = transforms[i].localPosition;
                }
                else
                {
                    origins[i] = transforms[i].position;
                }
            }
        }

        public void Execute()
        {
            if (!isOriginInitialized)
            {
                InitializeOrigins();
            }

            elapsedTime += Time.deltaTime;

            for (int i = 0; i < transforms.Length; i++)
            {
                float t = Ease.Value(TimelapseProportion(), easing);
                
                if (isCoordinateLocal)
                {
                    transforms[i].position = LerpPosition(transforms[i].localPosition, i, t);
                }
                else
                {
                    transforms[i].position = LerpPosition(transforms[i].position, i, t);
                }
                

                if (IsOver())
                {
                    SetFinalPosition(i);
                }
            }
        }

        private Vector3 LerpPosition(Vector3 actualPosition, int index, float proportion)
        {
            if (moveX)
            {
                actualPosition.x = Mathf.LerpUnclamped(origins[index].x, targetX, proportion);
            }

            if (moveY)
            {
                actualPosition.y = Mathf.LerpUnclamped(origins[index].y, targetY, proportion);
            }

            if (moveZ)
            {
                actualPosition.z = Mathf.LerpUnclamped(origins[index].z, targetZ, proportion);
            }

            return actualPosition;
        }

        private void SetFinalPosition(int index)
        {
            Vector3 position = isCoordinateLocal ? transforms[index].localPosition : transforms[index].position;

            if (moveX)
            {
                position.x = targetX;
            }

            if (moveY)
            {
                position.y = targetY;
            }

            if (moveZ)
            {
                position.z = targetZ;
            }

            if (isCoordinateLocal)
            {
                transforms[index].localPosition = position;
            }
            else
            {
                transforms[index].position = position;
            }
        }

        public void Reset()
        {
            elapsedTime = 0f;            
        }

        public bool IsOver()
        {
            return elapsedTime > duration;
        }

        public float TimelapseProportion()
        {
            return duration <= 0f ? 1f : elapsedTime / duration;
        }
    }    
}
