using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TimetableSystem
{
    public struct ImageColorTask : ITaskOrder
    {
        private readonly Image[] images;
        private readonly Color[] origins;
        private readonly float targetR;
        private readonly float targetG;
        private readonly float targetB;
        private readonly float targetA;

        private readonly bool changeR;
        private readonly bool changeG;
        private readonly bool changeB;
        private readonly bool changeA;

        private bool isOriginInitialized;

        private readonly float duration;
        private readonly float startTime;
        private float elapsedTime;
        private readonly EaseMode easing;

        public ImageColorTask(Image image, ColorChannelsData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            images = new Image[] { image };
            origins = new Color[1];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> colorValues = to.GetColorValues();
            changeR = colorValues.ContainsKey("r");
            changeG = colorValues.ContainsKey("g");
            changeB = colorValues.ContainsKey("b");
            changeA = colorValues.ContainsKey("a");

            targetR = changeR ? colorValues["r"] : 0f;
            targetG = changeG ? colorValues["g"] : 0f;
            targetB = changeB ? colorValues["b"] : 0f;
            targetA = changeA ? colorValues["a"] : 0f;
        }

        public ImageColorTask(Image[] images, ColorChannelsData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            this.images = images;
            origins = new Color[images.Length];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> colorValues = to.GetColorValues();
            changeR = colorValues.ContainsKey("r");
            changeG = colorValues.ContainsKey("g");
            changeB = colorValues.ContainsKey("b");
            changeA = colorValues.ContainsKey("a");

            targetR = changeR ? colorValues["r"] : 0f;
            targetG = changeG ? colorValues["g"] : 0f;
            targetB = changeB ? colorValues["b"] : 0f;
            targetA = changeA ? colorValues["a"] : 0f;
        }

        public float StartTime => startTime;

        public float Duration => duration;

        public float ElapsedTime => elapsedTime;

        private void InitializeOrigins()
        {
            isOriginInitialized = true;

            for (int i = 0; i < origins.Length; i++)
            {
                origins[i] = images[i].color;
            }
        }

        private Color LerpColor(Color actualColor, int index, float proportion)
        {
            if (changeR)
            {
                actualColor.r = Mathf.LerpUnclamped(origins[index].r, targetR, proportion);
            }

            if (changeG)
            {
                actualColor.g = Mathf.LerpUnclamped(origins[index].g, targetG, proportion);
            }

            if (changeB)
            {
                actualColor.b = Mathf.LerpUnclamped(origins[index].b, targetB, proportion);
            }

            if (changeA)
            {
                actualColor.a = Mathf.LerpUnclamped(origins[index].a, targetA, proportion);
            }

            return actualColor;
        }

        public void Execute()
        {
            if (!isOriginInitialized)
            {
                InitializeOrigins();
            }

            elapsedTime += Time.deltaTime;

            for (int i = 0; i < images.Length; i++)
            {
                float t = Ease.Value(TimelapseProportion(), easing);
                images[i].color = LerpColor(images[i].color, i, t);

                if (IsOver())
                {
                    SetFinalColor(i);
                }
            }
        }

        private void SetFinalColor(int index)
        {
            Color color = images[index].color;

            if (changeR)
            {
                color.r = targetR;
            }

            if (changeG)
            {
                color.g = targetG;
            }

            if (changeB)
            {
                color.b = targetB;
            }

            if (changeA)
            {
                color.a = targetA;
            }

            images[index].color = color;
        }

        public bool IsOver()
        {
            return elapsedTime > duration;
        }

        public void Reset()
        {
            elapsedTime = 0f;            
        }

        public float TimelapseProportion()
        {
            return duration <= 0f ? 1f : elapsedTime / duration;
        }
    }
}
