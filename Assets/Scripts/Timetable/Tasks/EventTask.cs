using System;

namespace TimetableSystem
{
    public struct EventTask : ITaskOrder
    {
        private readonly Action callback;
        private readonly float duration;
        private readonly float startTime;
        private float elapsedTime;        

        public EventTask(Action callback, float startTime)
        {
            this.callback = callback;
            this.startTime = startTime;
            duration = 0f;
            elapsedTime = 0f;
        }

        public float StartTime => startTime;

        public float Duration => duration;

        public float ElapsedTime => elapsedTime;

        public void Execute()
        {
            callback();
            elapsedTime = 1f;
        }

        public bool IsOver()
        {
            return elapsedTime > duration;
        }

        public void Reset()
        {
            elapsedTime = 0f;
        }

        public float TimelapseProportion()
        {
            return duration <= 0f ? 1f : elapsedTime / duration;
        }
    }
}
