using System.Collections.Generic;

namespace TimetableSystem
{
    public class ColorChannelsData
    {
        public float? R = null;
        public float? G = null;
        public float? B = null;
        public float? A = null;

        public Dictionary<string, float> GetColorValues()
        {
            Dictionary<string, float> valuesMap = new Dictionary<string, float>();

            if (R.HasValue)
            {
                valuesMap.Add("r", R.Value);
            }

            if (G.HasValue)
            {
                valuesMap.Add("g", G.Value);
            }

            if (B.HasValue)
            {
                valuesMap.Add("b", B.Value);
            }

            if (A.HasValue)
            {
                valuesMap.Add("a", A.Value);
            }

            return valuesMap;
        }
    }
}
