using System.Collections.Generic;

namespace TimetableSystem
{
    public class VectorData
    {        
        public float? X = null;
        public float? Y = null;
        public float? Z = null;

        public Dictionary<string, float> GetCoordinates()
        {
            Dictionary<string, float> valuesMap = new Dictionary<string, float>();

            if (X.HasValue)
            {
                valuesMap.Add("x", X.Value);
            }

            if (Y.HasValue)
            {
                valuesMap.Add("y", Y.Value);
            }

            if (Z.HasValue)
            {
                valuesMap.Add("z", Z.Value);
            }

            return valuesMap;
        }
    }    
}
