using UnityEngine;

namespace TimetableSystem
{
    public struct MaterialPropertyData
    {
        public int ValueInt;
        public float ValueFloat;
        public Vector4 ValueVector;
        public Color ValueColor;
    }
}
