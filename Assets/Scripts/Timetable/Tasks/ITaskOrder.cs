namespace TimetableSystem
{
    public interface ITaskOrder
    {
        public float StartTime { get; }
        public float Duration { get; }
        public float ElapsedTime { get; }

        public float TimelapseProportion();
        public void Execute();
        public void Reset();
        public bool IsOver();
    }
}
