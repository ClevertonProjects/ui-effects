using System.Collections.Generic;
using UnityEngine;

namespace TimetableSystem
{
    public struct RotationTask : ITaskOrder
    {
        private readonly Transform[] transforms;
        private readonly Vector3[] origins;
        private readonly float targetX;
        private readonly float targetY;
        private readonly float targetZ;

        private readonly bool rotateX;
        private readonly bool rotateY;
        private readonly bool rotateZ;

        private bool isOriginInitialized;

        private readonly float duration;
        private readonly float startTime;
        private float elapsedTime;
        private readonly EaseMode easing;

        public RotationTask(Transform transform, VectorData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            transforms = new Transform[] { transform };
            origins = new Vector3[1];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> coordinates = to.GetCoordinates();
            rotateX = coordinates.ContainsKey("x");
            rotateY = coordinates.ContainsKey("y");
            rotateZ = coordinates.ContainsKey("z");

            targetX = rotateX ? coordinates["x"] : 0f;
            targetY = rotateY ? coordinates["y"] : 0f;
            targetZ = rotateZ ? coordinates["z"] : 0f;
        }

        public RotationTask(Transform[] transforms, VectorData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            this.transforms = transforms;
            origins = new Vector3[transforms.Length];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> coordinates = to.GetCoordinates();
            rotateX = coordinates.ContainsKey("x");
            rotateY = coordinates.ContainsKey("y");
            rotateZ = coordinates.ContainsKey("z");

            targetX = rotateX ? coordinates["x"] : 0f;
            targetY = rotateY ? coordinates["y"] : 0f;
            targetZ = rotateZ ? coordinates["z"] : 0f;
        }

        public float Duration => duration;
        public float StartTime => startTime;
        public float ElapsedTime => elapsedTime;

        private void InitializeOrigins()
        {
            isOriginInitialized = true;

            for (int i = 0; i < origins.Length; i++)
            {
                origins[i] = transforms[i].localEulerAngles;
            }
        }

        private Vector3 LerpRotation(Vector3 actualRotation, int index, float proportion)
        {
            if (rotateX)
            {
                actualRotation.x = Mathf.LerpUnclamped(origins[index].x, targetX, proportion);
            }

            if (rotateY)
            {
                actualRotation.y = Mathf.LerpUnclamped(origins[index].y, targetY, proportion);
            }

            if (rotateZ)
            {
                actualRotation.z = Mathf.LerpUnclamped(origins[index].z, targetZ, proportion);
            }

            return actualRotation;
        }

        public void Execute()
        {
            if (!isOriginInitialized)
            {
                InitializeOrigins();
            }

            elapsedTime += Time.deltaTime;

            for (int i = 0; i < transforms.Length; i++)
            {
                float t = Ease.Value(TimelapseProportion(), easing);
                transforms[i].localEulerAngles = LerpRotation(transforms[i].localEulerAngles, i, t);

                if (IsOver())
                {
                    SetFinalRotation(i);
                }
            }
        }        

        private void SetFinalRotation(int index)
        {
            Vector3 rotation = transforms[index].localEulerAngles;

            if (rotateX)
            {
                rotation.x = targetX;
            }

            if (rotateY)
            {
                rotation.y = targetY;
            }

            if (rotateZ)
            {
                rotation.z = targetZ;
            }

            transforms[index].localEulerAngles = rotation;
        }

        public void Reset()
        {
            elapsedTime = 0f;            
        }

        public bool IsOver()
        {
            return elapsedTime > duration;
        }

        public float TimelapseProportion()
        {
            return duration <= 0f ? 1f : elapsedTime / duration;
        }
    }
}
