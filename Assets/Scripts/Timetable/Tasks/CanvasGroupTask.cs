using UnityEngine;

namespace TimetableSystem
{
    public class CanvasGroupTask : ITaskOrder
    {
        private readonly CanvasGroup[] canvasGroups;
        private readonly float[] fromAlpha;
        private readonly float targetAlpha;

        private readonly float duration;
        private readonly float startTime;
        private float elapsedTime;
        private readonly EaseMode easing;

        public CanvasGroupTask(CanvasGroup canvasGroup, CanvasGroupData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            canvasGroups = new CanvasGroup[] { canvasGroup };
            this.duration = duration;
            this.startTime = startTime;
            elapsedTime = 0f;
            this.easing = easing;

            fromAlpha = new float[] { canvasGroup.alpha };
            targetAlpha = to.Alpha;
        }

        public CanvasGroupTask(CanvasGroup[] canvasGroups, CanvasGroupData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            this.canvasGroups = canvasGroups;
            this.duration = duration;
            this.startTime = startTime;
            elapsedTime = 0f;
            this.easing = easing;

            fromAlpha = new float[canvasGroups.Length];
            for (int i = 0; i < fromAlpha.Length; i++)
            {
                fromAlpha[i] = canvasGroups[i].alpha;
            }

            targetAlpha = to.Alpha;
        }

        public float StartTime => startTime;

        public float Duration => duration;

        public float ElapsedTime => elapsedTime;

        public void Execute()
        {
            elapsedTime += Time.deltaTime;

            for (int i = 0; i < canvasGroups.Length; i++)
            {
                float t = Ease.Value(TimelapseProportion(), easing);
                canvasGroups[i].alpha = Mathf.LerpUnclamped(fromAlpha[i], targetAlpha, t);

                if (IsOver())
                {
                    canvasGroups[i].alpha = targetAlpha;
                }
            }
        }

        public bool IsOver()
        {
            return elapsedTime > duration;
        }

        public void Reset()
        {
            elapsedTime = 0f;
        }

        public float TimelapseProportion()
        {
            return duration <= 0f ? 1f : elapsedTime / duration;
        }
    }
}
