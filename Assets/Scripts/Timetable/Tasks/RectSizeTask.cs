using TimetableSystem;
using UnityEngine;
using System.Collections.Generic;

public class RectSizeTask : ITaskOrder
{
    private readonly RectTransform[] rectTransforms;
    private readonly Vector2[] origins;
    private readonly float targetX;
    private readonly float targetY;

    private readonly bool changeX;
    private readonly bool changeY;

    private bool isOriginInitialized;

    private readonly float duration;
    private readonly float startTime;
    private float elapsedTime;
    private readonly EaseMode easing;

    public RectSizeTask(RectTransform rectTransform, VectorData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
    {
        rectTransforms = new RectTransform[] { rectTransform };
        origins = new Vector2[1];
        this.duration = duration;
        this.startTime = startTime;
        isOriginInitialized = false;
        elapsedTime = 0f;
        this.easing = easing;

        Dictionary<string, float> coordinates = to.GetCoordinates();
        changeX = coordinates.ContainsKey("x");
        changeY = coordinates.ContainsKey("y");

        targetX = changeX ? coordinates["x"] : 0f;
        targetY = changeY ? coordinates["y"] : 0f;
    }

    public RectSizeTask(RectTransform[] rectTransforms, VectorData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
    {
        this.rectTransforms = rectTransforms;
        origins = new Vector2[rectTransforms.Length];
        this.duration = duration;
        this.startTime = startTime;
        isOriginInitialized = false;
        elapsedTime = 0f;
        this.easing = easing;

        Dictionary<string, float> coordinates = to.GetCoordinates();
        changeX = coordinates.ContainsKey("x");
        changeY = coordinates.ContainsKey("y");

        targetX = changeX ? coordinates["x"] : 0f;
        targetY = changeY ? coordinates["y"] : 0f;
    }

    public float StartTime => startTime;

    public float Duration => duration;

    public float ElapsedTime => elapsedTime;

    private void InitializeOrigins()
    {
        isOriginInitialized = true;

        for (int i = 0; i < origins.Length; i++)
        {
            origins[i] = rectTransforms[i].sizeDelta;
        }
    }

    private Vector2 LerpSize(Vector2 actualSize, int index, float proportion)
    {
        if (changeX)
        {
            actualSize.x = Mathf.LerpUnclamped(origins[index].x, targetX, proportion);
        }

        if (changeY)
        {
            actualSize.y = Mathf.LerpUnclamped(origins[index].y, targetY, proportion);
        }

        return actualSize;
    }

    public void Execute()
    {
        if (!isOriginInitialized)
        {
            InitializeOrigins();
        }

        elapsedTime += Time.deltaTime;

        for (int i = 0; i < rectTransforms.Length; i++)
        {
            float t = Ease.Value(TimelapseProportion(), easing);
            rectTransforms[i].sizeDelta = LerpSize(rectTransforms[i].sizeDelta, i, t);

            if (IsOver())
            {
                SetFinalSize(i);
            }
        }
    }

    private void SetFinalSize(int index)
    {
        Vector2 size = rectTransforms[index].sizeDelta;

        if (changeX)
        {
            size.x = targetX;
        }

        if (changeY)
        {
            size.y = targetY;
        }

        rectTransforms[index].sizeDelta = size;
    }

    public bool IsOver()
    {
        return elapsedTime > duration;
    }

    public void Reset()
    {
        elapsedTime = 0f;
    }

    public float TimelapseProportion()
    {
        return duration <= 0f ? 1f : elapsedTime / duration;
    }
}
