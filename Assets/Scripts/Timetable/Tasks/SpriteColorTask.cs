using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TimetableSystem
{
    public struct SpriteColorTask : ITaskOrder
    {
        private readonly SpriteRenderer[] spriteRenderers;
        private readonly Color[] origins;
        private readonly float targetR;
        private readonly float targetG;
        private readonly float targetB;
        private readonly float targetA;

        private readonly bool changeR;
        private readonly bool changeG;
        private readonly bool changeB;
        private readonly bool changeA;

        private bool isOriginInitialized;

        private readonly float duration;
        private readonly float startTime;
        private float elapsedTime;
        private readonly EaseMode easing;

        public SpriteColorTask(SpriteRenderer sr, ColorChannelsData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            spriteRenderers = new SpriteRenderer[] { sr };
            origins = new Color[1];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> colorValues = to.GetColorValues();
            changeR = colorValues.ContainsKey("r");
            changeG = colorValues.ContainsKey("g");
            changeB = colorValues.ContainsKey("b");
            changeA = colorValues.ContainsKey("a");

            targetR = changeR ? colorValues["r"] : 0f;
            targetG = changeG ? colorValues["g"] : 0f;
            targetB = changeB ? colorValues["b"] : 0f;
            targetA = changeA ? colorValues["a"] : 0f;
        }

        public SpriteColorTask(SpriteRenderer[] sr, ColorChannelsData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            spriteRenderers = sr;
            origins = new Color[spriteRenderers.Length];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> colorValues = to.GetColorValues();
            changeR = colorValues.ContainsKey("r");
            changeG = colorValues.ContainsKey("g");
            changeB = colorValues.ContainsKey("b");
            changeA = colorValues.ContainsKey("a");

            targetR = changeR ? colorValues["r"] : 0f;
            targetG = changeG ? colorValues["g"] : 0f;
            targetB = changeB ? colorValues["b"] : 0f;
            targetA = changeA ? colorValues["a"] : 0f;
        }

        public float StartTime => startTime;

        public float Duration => duration;

        public float ElapsedTime => elapsedTime;

        private void InitializeOrigins()
        {
            isOriginInitialized = true;

            for (int i = 0; i < origins.Length; i++)
            {
                origins[i] = spriteRenderers[i].color;
            }
        }

        private Color LerpColor(Color actualColor, int index, float proportion)
        {
            if (changeR)
            {
                actualColor.r = Mathf.LerpUnclamped(origins[index].r, targetR, proportion);
            }

            if (changeG)
            {
                actualColor.g = Mathf.LerpUnclamped(origins[index].g, targetG, proportion);
            }

            if (changeB)
            {
                actualColor.b = Mathf.LerpUnclamped(origins[index].b, targetB, proportion);
            }

            if (changeA)
            {
                actualColor.a = Mathf.LerpUnclamped(origins[index].a, targetA, proportion);
            }

            return actualColor;
        }

        public void Execute()
        {
            if (!isOriginInitialized)
            {
                InitializeOrigins();
            }

            elapsedTime += Time.deltaTime;

            for (int i = 0; i < spriteRenderers.Length; i++)
            {
                float t = Ease.Value(TimelapseProportion(), easing);
                spriteRenderers[i].color = LerpColor(spriteRenderers[i].color, i, t);

                if (IsOver())
                {
                    SetFinalColor(i);
                }
            }
        }

        private void SetFinalColor(int index)
        {
            Color color = spriteRenderers[index].color;

            if (changeR)
            {
                color.r = targetR;
            }

            if (changeG)
            {
                color.g = targetG;
            }

            if (changeB)
            {
                color.b = targetB;
            }

            if (changeA)
            {
                color.a = targetA;
            }

            spriteRenderers[index].color = color;
        }

        public bool IsOver()
        {
            return elapsedTime > duration;
        }

        public void Reset()
        {
            elapsedTime = 0f;            
        }

        public float TimelapseProportion()
        {
            return duration <= 0f ? 1f : elapsedTime / duration;
        }
    }
}
