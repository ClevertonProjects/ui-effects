using System.Collections.Generic;
using UnityEngine;

namespace TimetableSystem
{
    public struct ScaleTask : ITaskOrder
    {
        private readonly Transform[] transforms;
        private readonly Vector3[] origins;
        private readonly float targetX;
        private readonly float targetY;
        private readonly float targetZ;

        private readonly bool scaleX;
        private readonly bool scaleY;
        private readonly bool scaleZ;

        private bool isOriginInitialized;

        private readonly float duration;
        private readonly float startTime;
        private float elapsedTime;
        private readonly EaseMode easing;

        public ScaleTask(Transform transform, VectorData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            transforms = new Transform[] { transform };
            origins = new Vector3[1];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> coordinates = to.GetCoordinates();
            scaleX = coordinates.ContainsKey("x");
            scaleY = coordinates.ContainsKey("y");
            scaleZ = coordinates.ContainsKey("z");

            targetX = scaleX ? coordinates["x"] : 1f;
            targetY = scaleY ? coordinates["y"] : 1f;
            targetZ = scaleZ ? coordinates["z"] : 1f;
        }

        public ScaleTask(Transform[] transforms, VectorData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            this.transforms = transforms;
            origins = new Vector3[transforms.Length];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> coordinates = to.GetCoordinates();
            scaleX = coordinates.ContainsKey("x");
            scaleY = coordinates.ContainsKey("y");
            scaleZ = coordinates.ContainsKey("z");

            targetX = scaleX ? coordinates["x"] : 1f;
            targetY = scaleY ? coordinates["y"] : 1f;
            targetZ = scaleZ ? coordinates["z"] : 1f;
        }

        public float Duration => duration;
        public float StartTime => startTime;
        public float ElapsedTime => elapsedTime;

        private void InitializeOrigins()
        {
            isOriginInitialized = true;

            for (int i = 0; i < origins.Length; i++)
            {
                origins[i] = transforms[i].localScale;
            }
        }

        public void Execute()
        {
            if (!isOriginInitialized)
            {
                InitializeOrigins();
            }

            elapsedTime += Time.deltaTime;

            for (int i = 0; i < transforms.Length; i++)
            {
                float t = Ease.Value(TimelapseProportion(), easing);
                transforms[i].localScale = LerpScale(transforms[i].localScale, i, t);

                if (IsOver())
                {
                    SetFinalScale(i);
                }
            }
        }

        private Vector3 LerpScale(Vector3 actualScale, int index, float proportion)
        {
            if (scaleX)
            {
                actualScale.x = Mathf.LerpUnclamped(origins[index].x, targetX, proportion);
            }

            if (scaleY)
            {
                actualScale.y = Mathf.LerpUnclamped(origins[index].y, targetY, proportion);
            }

            if (scaleZ)
            {
                actualScale.z = Mathf.LerpUnclamped(origins[index].z, targetZ, proportion);
            }

            return actualScale;
        }

        private void SetFinalScale(int index)
        {
            Vector3 scale = transforms[index].localScale;

            if (scaleX)
            {
                scale.x = targetX;
            }

            if (scaleY)
            {
                scale.y = targetY;
            }

            if (scaleZ)
            {
                scale.z = targetZ;
            }

            transforms[index].localScale = scale;
        }

        public void Reset()
        {
            elapsedTime = 0f;          
        }

        public bool IsOver()
        {
            return elapsedTime > duration;
        }

        public float TimelapseProportion()
        {
            return duration <= 0f ? 1f : elapsedTime / duration;
        }
    }
}
