using System.Collections.Generic;
using UnityEngine;

namespace TimetableSystem
{
    public struct Move2DTask : ITaskOrder
    {
        private readonly RectTransform[] rectTransforms;
        private readonly Vector2[] origins;
        private readonly float targetX;
        private readonly float targetY;

        private readonly bool moveX;
        private readonly bool moveY;

        private bool isOriginInitialized;

        private readonly float duration;
        private readonly float startTime;
        private float elapsedTime;
        private readonly EaseMode easing;

        public Move2DTask(RectTransform rectTransform, VectorData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            rectTransforms = new RectTransform[] { rectTransform };
            origins = new Vector2[1];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> coordinates = to.GetCoordinates();
            moveX = coordinates.ContainsKey("x");
            moveY = coordinates.ContainsKey("y");

            targetX = moveX ? coordinates["x"] : 0f;
            targetY = moveY ? coordinates["y"] : 0f;
        }

        public Move2DTask(RectTransform[] rectTransforms, VectorData to, float duration, float startTime, EaseMode easing = EaseMode.Linear)
        {
            this.rectTransforms = rectTransforms;
            origins = new Vector2[rectTransforms.Length];
            this.duration = duration;
            this.startTime = startTime;
            isOriginInitialized = false;
            elapsedTime = 0f;
            this.easing = easing;

            Dictionary<string, float> coordinates = to.GetCoordinates();
            moveX = coordinates.ContainsKey("x");
            moveY = coordinates.ContainsKey("y");

            targetX = moveX ? coordinates["x"] : 0f;
            targetY = moveY ? coordinates["y"] : 0f;
        }

        public float StartTime => startTime;

        public float Duration => duration;

        public float ElapsedTime => elapsedTime;

        private void InitializeOrigins()
        {
            isOriginInitialized = true;

            for (int i = 0; i < origins.Length; i++)
            {
                origins[i] = rectTransforms[i].anchoredPosition;
            }
        }

        private Vector2 LerpPosition(Vector2 actualPosition, int index, float proportion)
        {
            if (moveX)
            {
                actualPosition.x = Mathf.LerpUnclamped(origins[index].x, targetX, proportion);
            }

            if (moveY)
            {
                actualPosition.y = Mathf.LerpUnclamped(origins[index].y, targetY, proportion);
            }

            return actualPosition;
        }

        public void Execute()
        {
            if (!isOriginInitialized)
            {
                InitializeOrigins();
            }

            elapsedTime += Time.deltaTime;

            for (int i = 0; i < rectTransforms.Length; i++)
            {
                float t = Ease.Value(TimelapseProportion(), easing);
                rectTransforms[i].anchoredPosition = LerpPosition(rectTransforms[i].anchoredPosition, i, t);

                if (IsOver())
                {
                    SetFinalPosition(i);
                }
            }
        }

        private void SetFinalPosition(int index)
        {
            Vector2 position = rectTransforms[index].anchoredPosition;

            if (moveX)
            {
                position.x = targetX;
            }

            if (moveY)
            {
                position.y = targetY;
            }

            rectTransforms[index].anchoredPosition = position;
        }

        public bool IsOver()
        {
            return elapsedTime > duration;
        }

        public void Reset()
        {
            elapsedTime = 0f;            
        }

        public float TimelapseProportion()
        {
            return duration <= 0f ? 1f : elapsedTime / duration;
        }
    }
}
