using TimetableSystem;
using UnityEngine;

public class SwirlActions : MonoBehaviour
{
    [SerializeField] private Material mat;
    private Timetable timeTable;

    // Start is called before the first frame update
    void Start()
    {
        mat.SetVector("_Center", new Vector4(0.5f, 0.5f, 0f, 0f));
        mat.SetFloat("_Angle", 0);
        mat.SetFloat("_ColorTransition", 0f);

        timeTable = GetComponent<Timetable>();

        timeTable.Loop = true;        
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_ColorTransition"), 1f, 1f, 0.5f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Angle"), 5f, 2f, 1.5f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Center"), new Vector4(0.5f, 2.2f, 0f, 0f), 1.5f, 3f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_ColorTransition"), 0f, 0.5f, 3.5f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Angle"), 0f, 0f, 4.5f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Center"), new Vector4(0.5f, 0.5f, 0f, 0f), 0f, 4.5f));

        timeTable.Run();
    }    
}
