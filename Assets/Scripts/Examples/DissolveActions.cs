using UnityEngine;
using TimetableSystem;

public class DissolveActions : MonoBehaviour
{
    [SerializeField] private Material mat;
    private Timetable timeTable;

    // Start is called before the first frame update
    void Start()
    {
        // Set the initial material values
        mat.SetFloat("_CutOff", 1f);
        mat.SetFloat("_EdgeSize", 0.1f);

        timeTable = GetComponent<Timetable>();

        timeTable.Loop = true;
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_CutOff"), 0f, 2f, 0.5f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_EdgeSize"), 0f, 0.3f, 2.3f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_EdgeSize"), 0.1f, 0.25f, 3f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_CutOff"), 1f, 2f, 3.2f));

        timeTable.Run();
    }    
}
