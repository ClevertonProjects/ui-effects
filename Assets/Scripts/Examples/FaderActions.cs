using TimetableSystem;
using UnityEngine;

public class FaderActions : MonoBehaviour
{
    [SerializeField] private Material mat;
    private Timetable timeTable;

    // Start is called before the first frame update
    void Start()
    {
        // Set the initial material values
        mat.SetFloat("_CutOff", 1f);
        mat.SetFloat("_EdgeSize", 0.1f);
        mat.SetColor("_Color", Color.clear);

        timeTable = GetComponent<Timetable>();

        timeTable.Loop = true;
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Color"), new Color(0f, 0f, 0f, 1f), 1.5f, 0.5f, EaseMode.QuadraticInOut));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_CutOff"), 0f, 2f, 2.0f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_EdgeSize"), 0f, 0.3f, 3.7f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_EdgeSize"), 0.1f, 0.25f, 4.5f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_CutOff"), 1f, 2f, 4.7f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Color"), Color.clear, 1.5f, 6.7f, EaseMode.QuadraticInOut));

        timeTable.Run();
    }
}
