using TimetableSystem;
using UnityEngine;

public class CircleSizeActions : MonoBehaviour
{
    [SerializeField] private Material mat;
    private Timetable timeTable;

    // Start is called before the first frame update
    void Start()
    {
        timeTable = GetComponent<Timetable>();

        mat.SetFloat("_VRadius", 0.5f);

        timeTable.Loop = true;
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_VRadius"), 0.75f, 1.5f, 0.5f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_VRadius"), 0.2f, 2f, 2.3f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_VRadius"), 0.5f, 1f, 4.6f));

        timeTable.Run();
    }
}
