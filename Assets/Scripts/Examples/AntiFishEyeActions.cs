using TimetableSystem;
using UnityEngine;

public class AntiFishEyeActions : MonoBehaviour
{
    [SerializeField] private Material mat;
    private Timetable timeTable;

    // Start is called before the first frame update
    void Start()
    {
        mat.SetFloat("_Strength", 0.5f);

        timeTable = GetComponent<Timetable>();

        timeTable.Loop = true;
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Strength"), 0.2f, 2.5f, 0.5f, EaseMode.CircularOut));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Strength"), 0.5f, 0.6f, 3.0f, EaseMode.BounceOut));

        timeTable.Run();
    }
}
