using UnityEngine;
using TimetableSystem;

public class CircleSoftness : MonoBehaviour
{
    [SerializeField] private Material mat;
    private Timetable timeTable;

    // Start is called before the first frame update
    void Start()
    {
        timeTable = GetComponent<Timetable>();

        mat.SetFloat("_VSoft", 0f);

        timeTable.Loop = true;
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_VSoft"), 1f, 2f, 0.5f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_VSoft"), 0f, 2f, 3f));

        timeTable.Run();
    }    
}
