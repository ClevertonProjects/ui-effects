using TimetableSystem;
using UnityEngine;

public class TwirlActions : MonoBehaviour
{
    [SerializeField] private Material mat;
    private Timetable timeTable;

    // Start is called before the first frame update
    void Start()
    {        
        mat.SetFloat("_Strength", 0);        

        timeTable = GetComponent<Timetable>();

        timeTable.Loop = true;        
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Strength"), 1.5f, 2f, 0.5f, EaseMode.CubicOut));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Strength"), -1.5f, 2f, 2.5f, EaseMode.CubicOut));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Strength"), 0f, 1f, 4.5f, EaseMode.ElasticOut));

        timeTable.Run();
    }
}
