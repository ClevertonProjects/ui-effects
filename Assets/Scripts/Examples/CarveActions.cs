using TimetableSystem;
using UnityEngine;

public class CarveActions : MonoBehaviour
{
    [SerializeField] private Material mat;
    private Timetable timeTable;

    // Start is called before the first frame update
    void Start()
    {
        mat.SetVector("_CarveConfig", new Vector4(0.5f, 0.5f, 0.2f, 0.2f));        

        timeTable = GetComponent<Timetable>();

        timeTable.Loop = true;        
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_CarveConfig"), new Vector4(0.5f, 0.5f, 0.5f, 0.2f), 1f, 0.5f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_CarveConfig"), new Vector4(0.5f, 0.5f, 0.2f, 0.5f), 1f, 1.8f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_CarveConfig"), new Vector4(0.15f, 0.5f, 0.2f, 0.2f), 1f, 3.1f));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_CarveConfig"), new Vector4(0.5f, 0.5f, 0.2f, 0.2f), 1f, 4.4f));

        timeTable.Run();
    }
}
