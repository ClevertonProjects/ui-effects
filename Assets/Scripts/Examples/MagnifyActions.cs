using UnityEngine;
using TimetableSystem;

public class MagnifyActions : MonoBehaviour
{
    [SerializeField] private Material mat;
    private Timetable timeTable;

    // Start is called before the first frame update
    void Start()
    {
        mat.SetFloat("_Strength", 0f);

        timeTable = GetComponent<Timetable>();

        timeTable.Loop = true;
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Strength"), 3.5f, 3f, 0.5f, EaseMode.SineOut));
        timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Strength"), 0f, 3f, 4.5f, EaseMode.QuarticIn));

        timeTable.Run();
    }    
}
