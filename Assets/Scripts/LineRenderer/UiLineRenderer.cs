using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Sprites;
using UnityEngine.UI;

namespace LineRenderer
{
    public class UiLineRenderer : Graphic
    {
        public enum FillDirections { Right, Left }

        public Sprite LineSprite;
        [SerializeField]
        private FillDirections _fillDirection;
        [Range(0, 1), SerializeField]
        private float _fillAmount = 1f;
        public GridCoords GridSize;
        [SerializeField] List<LineParameters> _lines = new List<LineParameters>();
        public float Thickness = 32f;

        private float _width;
        private float _height;
        private Vector2 _cellSize;

        private Vector3[] _vertices = new Vector3[4];
        private Vector2[] _uvs = new Vector2[4];
        private Vector2[] _slicedVertices = new Vector2[4];
        private Vector2[] _slicedUVs = new Vector2[4];

        #region Functions

        public void AddLines(int initialRow, int initialCollunm, int finalRow, int finalCollunm)
        {
            AddLines(initialRow, initialCollunm, finalRow, finalCollunm, Color.white, true);
        }

        public void AddLines(int initialRow, int initialCollunm, int finalRow, int finalCollunm, Color lineColor)
        {
            AddLines(initialRow, initialCollunm, finalRow, finalCollunm, lineColor, true);
        }

        public void AddLines(int initialRow, int initialCollunm, int finalRow, int finalCollunm, Color lineColor, bool isVisible)
        {
            GridCoords initialPoint = new GridCoords()
            {
                Row = initialRow,
                Column = initialCollunm
            };

            GridCoords finalPoint = new GridCoords()
            {
                Row = finalRow,
                Column = finalCollunm
            };

            LineParameters line = new LineParameters()
            {
                InitialPoint = initialPoint,
                FinalPoint = finalPoint,
                LineColor = lineColor,
                IsVisible = isVisible
            };

            _lines.Add(line);
        }

        public void Clear()
        {
            FillAmount = 0;
            _lines.Clear();
        }

        public void SetLineVisibility(int lineIndex, bool isVisible)
        {
            LineParameters newLineState = _lines[lineIndex];
            newLineState.IsVisible = isVisible;

            _lines[lineIndex] = newLineState;
            SetAllDirty();
        }

        #endregion

        #region Properties

        public FillDirections FillDirection
        {
            get { return _fillDirection; }
            set
            {
                if (value != _fillDirection)
                {
                    _fillDirection = value;
                    SetVerticesDirty();
                }
            }
        }

        public float FillAmount
        {
            get { return _fillAmount; }
            set
            {
                float clampedValue = Mathf.Clamp01(value);

                if (clampedValue != _fillAmount)
                {
                    _fillAmount = clampedValue;
                    SetVerticesDirty();
                }
            }
        }

        public int LinesAmount
        {
            get { return _lines.Count; }
        }

        public override Texture mainTexture
        {
            get
            {
                if (LineSprite != null)
                    return LineSprite.texture;

                return base.mainTexture;
            }
        }

        #endregion

        private bool HasBorder()
        {
            if (LineSprite != null)
            {
                Vector4 v = LineSprite.border;
                return v.sqrMagnitude > 0f;
            }

            return false;
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            vh.Clear();

            _width = rectTransform.rect.width;
            _height = rectTransform.rect.height;

            _cellSize.x = _width / GridSize.Column;
            _cellSize.y = _height / GridSize.Row;

            if (_lines == null || GridSize.Row == 0 || GridSize.Column == 0)
                return;

            if (LineSprite != null)
            {
                GenerateSlicedFilledSprite(vh);
            }
            else
            {
                int linesAmount = _lines.Count;

                for (int line = 0; line < linesAmount; line++)
                {
                    if (_lines[line].InitialPoint == _lines[line].FinalPoint) continue;
                    if (!_lines[line].IsVisible) continue;

                    int startIndex = vh.currentVertCount;
                    DrawPointVertices(_lines[line].InitialPoint, _lines[line].FinalPoint, vh, _lines[line].LineColor);

                    vh.AddTriangle(startIndex, startIndex + 1, startIndex + 3);
                    vh.AddTriangle(startIndex + 3, startIndex + 2, startIndex);
                }
            }
        }

        private Vector3 GetPlaneNormal(GridCoords fromPoint, GridCoords toPoint, out float planeAngle)
        {
            Vector2 dir = GridCoords.GetDirection(fromPoint, toPoint).normalized;

            // Convert from quadrant I to IV, because the usual origin is the top left corner.
            dir *= new Vector2(1, -1);

            float sizeRatio = _cellSize.x / _cellSize.y;

            // Check if the grid cells aren't squared.
            if (sizeRatio != 1f)
            {
                dir.x *= sizeRatio;
            }

            planeAngle = Vector2.SignedAngle(Vector2.right, dir);
            Vector3 normal = Vector3.Cross(dir, Vector3.back);
            return normal;
        }

        private void DrawPointVertices(GridCoords initialPoint, GridCoords endPoint, VertexHelper vh, Color lineColor)
        {
            Vector3 normal = GetPlaneNormal(initialPoint, endPoint, out float planeAngle);

            float halfThickness = Thickness / 2;
            float originX = -_width / 2 + _cellSize.x / 2;
            float originY = _height / 2 - _cellSize.y / 2;

            Vector2 origin = new Vector2(originX, originY);
            Vector3 position;
            Vector3 thicknessOffset = halfThickness * normal;
            UIVertex vertex = UIVertex.simpleVert;
            vertex.color = lineColor;

            float posX = origin.x + initialPoint.Column * _cellSize.x;
            float posY = origin.y - initialPoint.Row * _cellSize.y;
            position = new Vector3(posX, posY);

            vertex.position = position + thicknessOffset;
            vh.AddVert(vertex);

            vertex.position = position - thicknessOffset;
            vh.AddVert(vertex);

            posX = origin.x + endPoint.Column * _cellSize.x;
            posY = origin.y - endPoint.Row * _cellSize.y;
            position = new Vector3(posX, posY);

            vertex.position = position + thicknessOffset;
            vh.AddVert(vertex);

            vertex.position = position - thicknessOffset;
            vh.AddVert(vertex);
        }

        private Rect GetLineRect(GridCoords initialPoint, GridCoords endPoint, out Vector3 initialCellPosition, out float planeAngle)
        {
            Vector3 normal = GetPlaneNormal(initialPoint, endPoint, out planeAngle);

            float halfThickness = Thickness / 2;
            float originX = -_width / 2 + _cellSize.x / 2;
            float originY = _height / 2 - _cellSize.y / 2;

            Vector3[] vertex = new Vector3[4];
            Vector2 origin = new Vector2(originX, originY);
            Vector3 position;
            Vector3 thicknessOffset = halfThickness * normal;

            float posX = origin.x + initialPoint.Column * _cellSize.x;
            float posY = origin.y - initialPoint.Row * _cellSize.y;
            position = new Vector3(posX, posY);

            vertex[0] = position - thicknessOffset;
            vertex[1] = position + thicknessOffset;

            posX = origin.x + endPoint.Column * _cellSize.x;
            posY = origin.y - endPoint.Row * _cellSize.y;
            position = new Vector3(posX, posY);

            vertex[2] = position - thicknessOffset;
            vertex[3] = position + thicknessOffset;

            float lineWidth = (vertex[2] - vertex[0]).magnitude;
            initialCellPosition = new Vector3(origin.x + initialPoint.Column * _cellSize.x, origin.y - initialPoint.Row * _cellSize.y);
            return new Rect(0, -Thickness / 2, lineWidth, Thickness);
        }

        private Vector4 GetAdjustedBorders(Vector4 border, Rect adjustedRect)
        {
            Rect originalRect = new Rect(adjustedRect);

            for (int axis = 0; axis <= 1; axis++)
            {
                float borderScaleRatio;

                // The adjusted rect (adjusted for pixel correctness)
                // may be slightly larger than the original rect.
                // Adjust the border to match the adjustedRect to avoid
                // small gaps between borders.
                if (originalRect.size[axis] != 0)
                {
                    borderScaleRatio = adjustedRect.size[axis] / originalRect.size[axis];
                    border[axis] *= borderScaleRatio;
                    border[axis + 2] *= borderScaleRatio;
                }

                // If the rect is smaller than the combined borders, then there's not room for the borders at their normal size.
                // In order to avoid artefacts with overlapping borders, we scale the borders down to fit.
                float combinedBorders = border[axis] + border[axis + 2];
                if (adjustedRect.size[axis] < combinedBorders && combinedBorders != 0)
                {
                    borderScaleRatio = adjustedRect.size[axis] / combinedBorders;
                    border[axis] *= borderScaleRatio;
                    border[axis + 2] *= borderScaleRatio;
                }
            }

            return border;
        }

        private void GenerateSlicedFilledSprite(VertexHelper vh)
        {
            vh.Clear();

            if (_fillAmount < 0.001f)
                return;

            int linesAmount = _lines.Count;

            for (int line = 0; line < linesAmount; line++)
            {
                if (_lines[line].InitialPoint == _lines[line].FinalPoint) continue;
                if (!_lines[line].IsVisible) continue;

                Rect rect = GetLineRect(_lines[line].InitialPoint, _lines[line].FinalPoint, out Vector3 cellPosition, out float planeAngle);
                Quaternion rotation = Quaternion.Euler(0f, 0f, planeAngle);
                Vector4 outer = DataUtility.GetOuterUV(LineSprite);
                Vector4 padding = DataUtility.GetPadding(LineSprite);                

                Vector4 inner = DataUtility.GetInnerUV(LineSprite);
                Vector4 border = GetAdjustedBorders(LineSprite.border, rect);

                _slicedVertices[0] = new Vector2(padding.x, padding.y);
                _slicedVertices[3] = new Vector2(rect.width - padding.z, rect.height - padding.w);

                _slicedVertices[1].x = border.x;
                _slicedVertices[1].y = border.y;

                _slicedVertices[2].x = rect.width - border.z;
                _slicedVertices[2].y = rect.height - border.w;

                for (int i = 0; i < 4; i++)
                {
                    _slicedVertices[i].x += rect.x;
                    _slicedVertices[i].y += rect.y;
                }

                _slicedUVs[0] = new Vector2(outer.x, outer.y);
                _slicedUVs[1] = new Vector2(inner.x, inner.y);
                _slicedUVs[2] = new Vector2(inner.z, inner.w);
                _slicedUVs[3] = new Vector2(outer.z, outer.w);

                float rectStartPos = _slicedVertices[0].x;
                float totalSize = (_slicedVertices[3].x - _slicedVertices[0].x);
                float inverseTotalSize = totalSize > 0f ? 1 / totalSize : 1;

                for (int x = 0; x < 3; x++)
                {
                    int x2 = x + 1;

                    float sliceStart;
                    float sliceEnd;

                    switch (_fillDirection)
                    {
                        case FillDirections.Right:
                            sliceStart = (_slicedVertices[x].x - rectStartPos) * inverseTotalSize;
                            sliceEnd = (_slicedVertices[x2].x - rectStartPos) * inverseTotalSize;
                            break;

                        case FillDirections.Left:
                            sliceStart = 1 - (_slicedVertices[x2].x - rectStartPos) * inverseTotalSize;
                            sliceEnd = 1 - (_slicedVertices[x].x - rectStartPos) * inverseTotalSize;
                            break;

                        default:
                            sliceStart = sliceEnd = 0f;
                            break;
                    }



                    for (int y = 0; y < 3; y++)
                    {
                        int y2 = y + 1;

                        Vector4 vertices = new Vector4(_slicedVertices[x].x, _slicedVertices[y].y, _slicedVertices[x2].x, _slicedVertices[y2].y);
                        Vector4 uvs = new Vector4(_slicedUVs[x].x, _slicedUVs[y].y, _slicedUVs[x2].x, _slicedUVs[y2].y);
                        float fillAmount = Mathf.Min((_fillAmount - sliceStart) / (sliceEnd - sliceStart), 1);

                        if (sliceStart >= _fillAmount)
                            continue;

                        GenerateFilledSprite(vh, vertices, uvs, fillAmount, cellPosition, rotation, _lines[line].LineColor);
                    }
                }
            }
        }

        private void GenerateFilledSprite(VertexHelper vh, Vector4 vertices, Vector4 uvs, float fillAmount, Vector3 cellPosition, Quaternion rotation, Color lineColor)
        {
            if (_fillAmount < 0.001f)
                return;

            float uvLeft = uvs.x;
            float uvBottom = uvs.y;
            float uvRight = uvs.z;
            float uvTop = uvs.w;

            if (_fillAmount < 1f)
            {
                if (_fillDirection == FillDirections.Left)
                {
                    vertices.x = vertices.z - (vertices.z - vertices.x) * fillAmount;
                    uvLeft = uvRight - (uvRight - uvLeft) * fillAmount;
                }
                else
                {
                    vertices.z = vertices.x + (vertices.z - vertices.x) * fillAmount;
                    uvRight = uvLeft + (uvRight - uvLeft) * fillAmount;
                }
            }

            _vertices[0] = new Vector3(vertices.x, vertices.y);
            _vertices[1] = new Vector3(vertices.x, vertices.w);
            _vertices[2] = new Vector3(vertices.z, vertices.w);
            _vertices[3] = new Vector3(vertices.z, vertices.y);

            Matrix4x4 m = Matrix4x4.identity;
            m.SetTRS(cellPosition, rotation, Vector3.one);

            _uvs[0] = new Vector2(uvLeft, uvBottom);
            _uvs[1] = new Vector2(uvLeft, uvTop);
            _uvs[2] = new Vector2(uvRight, uvTop);
            _uvs[3] = new Vector2(uvRight, uvBottom);

            int startIndex = vh.currentVertCount;

            for (int i = 0; i < 4; i++)
            {
                _vertices[i] = m.MultiplyPoint3x4(_vertices[i]);
                vh.AddVert(_vertices[i], lineColor, _uvs[i]);
            }

            vh.AddTriangle(startIndex, startIndex + 1, startIndex + 2);
            vh.AddTriangle(startIndex + 2, startIndex + 3, startIndex);
        }
    }
}
