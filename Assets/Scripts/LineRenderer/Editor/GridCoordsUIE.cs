using UnityEditor;
using UnityEngine;
using LineRenderer;

[CustomPropertyDrawer(typeof(GridCoords))]
public class GridCoordsUIE : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        Rect rowLabelRect = new Rect(position.x, position.y, 30, position.height);
        Rect rowFieldRect = new Rect(position.x + rowLabelRect.width, position.y, 60, position.height);
        Rect columnLabelRect = new Rect(position.x + rowLabelRect.width + rowFieldRect.width + 10, position.y, 50, position.height);
        Rect columnFieldRect = new Rect(columnLabelRect.x + columnLabelRect.width, position.y, 60, position.height);

        EditorGUI.LabelField(rowLabelRect, "Row");                
        EditorGUI.PropertyField(rowFieldRect, property.FindPropertyRelative("Row"), GUIContent.none);
        EditorGUI.LabelField(columnLabelRect, "Column");        
        EditorGUI.PropertyField(columnFieldRect, property.FindPropertyRelative("Column"), GUIContent.none);

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }    
}
