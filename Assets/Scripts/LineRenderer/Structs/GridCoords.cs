using UnityEngine;

namespace LineRenderer
{
    [System.Serializable]
    public struct GridCoords
    {
        public int Column;
        public int Row;

        public static Vector2 GetDirection(GridCoords from, GridCoords to)
        {
            return new Vector2(to.Column - from.Column, to.Row - from.Row);
        }

        public override bool Equals(object obj)
        {
            return obj is GridCoords coords &&
                   Column == coords.Column &&
                   Row == coords.Row;
        }

        public override int GetHashCode()
        {
            int hashCode = 656739706;
            hashCode = hashCode * -1521134295 + Column.GetHashCode();
            hashCode = hashCode * -1521134295 + Row.GetHashCode();
            return hashCode;
        }

        public static bool operator == (GridCoords a, GridCoords b)
        {
            bool result = a.Column == b.Column && a.Row == b.Row;
            return result;
        }

        public static bool operator != (GridCoords a, GridCoords b)
        {
            bool result = a.Column != b.Column || a.Row != b.Row;
            return result;
        }
    }
}
