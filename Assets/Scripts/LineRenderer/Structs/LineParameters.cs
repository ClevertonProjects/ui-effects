using UnityEngine;

namespace LineRenderer
{
    [System.Serializable]
    public struct LineParameters
    {
        public GridCoords InitialPoint;
        public GridCoords FinalPoint;
        public Color LineColor;

        public bool IsVisible;
    }
}
