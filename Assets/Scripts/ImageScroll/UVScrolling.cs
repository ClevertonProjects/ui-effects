using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVScrolling : MonoBehaviour
{    
    public enum UvType { SingleLayer, MultiLayer }

    public float Speed;
    private float offset;
    private int offsetId;

    public Material UvMat;
    public UvType UvLayerType;

    private float textureProp;
    private float initialUv;
    private float rowCount;

    private bool stop;

    private void Start()
    {
        textureProp = UvMat.GetFloat("_UVClampX");
        initialUv = UvMat.GetFloat("_InitialX");

        if (UvLayerType == UvType.MultiLayer)
            rowCount = UvMat.GetFloat("_RowCount");

        offsetId = Shader.PropertyToID("_OffsetX");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (stop) return;
        ScrollUV();
    }

    private void ScrollUV ()
    {
        offset += Speed * Time.fixedDeltaTime;        

        if (UvLayerType == UvType.MultiLayer)
        {
            float heightFactor = textureProp * rowCount;
            offset = (offset % heightFactor) + initialUv;
        }

        offset -= Mathf.Floor(offset);

        UvMat.SetFloat(offsetId, offset);        
    }

    public void ToggleStop ()
    {
        stop = !stop;
    }
}
