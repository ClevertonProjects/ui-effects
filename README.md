# UI Effects

This repository has a collection of effects and components used for animation on UI and general GameObjects

![uieffects](https://bitbucket.org/ClevertonProjects/ui-effects/raw/84b203d4bd3cd51921f317c7eecf67ef6bf64a17/Images/ImageFX.gif)

---

## Line Renderer

A UI component used to create lines in a grid

![linerenderer](https://bitbucket.org/ClevertonProjects/ui-effects/raw/0a9720a06bc0f1328364ba6d31cffccbf3718315/Images/LineRenderer.png)

This component has the same base functionality from the Image component (Color, Sprite, Fill). The grid configuration section has the line start and end grid coordinates, the cells size is calculated based on the width and height of the RectTransform.

## Timetable

A component used to create a sequence of scheduled events or the interpolation of components' properties.

```c#
timeTable.Loop = true;       
timeTable.AddTask(new Move2DTask(rectTransform, new() { X = 300, Y = 100 }, 2f, 0f));
timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_ColorTransition"), 1f, 1f, 0.5f));
timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Angle"), 5f, 2f, 1.5f));
timeTable.AddTask(new MaterialPropertyTask(mat, Shader.PropertyToID("_Center"), new Vector4(0.5f, 2.2f, 0f, 0f), 1.5f, 3f));

timeTable.Run();
```

The parameters of a new task may vary, accordingly to the type of task, but generally they have:    

* The interpolation duration
* The elapsed time to start the interpolation
* An optional easing model.

```c#
AddTask(...(..., ..., float duration, float startTime, EaseMode easing = EaseMode.Linear));
```

The components/properties supported in the timetable are:

* EventTask: Events (simple void without parameters, or a lambda expression)
* ImageColorTask: UI Image color
* MaterialPropertyTask: Material property (int, float, Vector4, Color)
* Move2DTask: UI RectTransform position
* Move3DTask: Transform position
* RotationTask: Any Transform euler angles
* ScaleTask: Any Transform scale
* SpriteColorTask: SpriteRenderer color

## UI Shaders

A collection of shaders compatible with UI Images.

![uieffects](https://bitbucket.org/ClevertonProjects/ui-effects/raw/84b203d4bd3cd51921f317c7eecf67ef6bf64a17/Images/ImageFX.gif)

Some of the shaders available are:

* GaussianBlur: applies a soft blur to images
* UIScrolling / UIScrollMultiLayer: an infinite image scroll, compatible with a texture atlas with the UV in a single or multiple lines
* DissolveEffect: creates a disolving effect based on a B&W pattern texture
* AnimatedTexture: animates a spritesheet, single or multiline, through shader

Note: all the animations in the example scene were created with the Timetable component